const { app, BrowserWindow, ipcMain, nativeTheme, globalShortcut } = require("electron");
const Datastore = require("nedb");
const path = require("path");
const nodemailer = require("nodemailer");
const CryptoJS = require('crypto-js');
const fs = require('fs');
const { dialog } = require("electron");

const transporter = nodemailer.createTransport({
  service: "Gmail", // Replace with your email service provider
  auth: {
    user: "alisedsmith@gmail.com", // Replace with your email address
    pass: "Khan7860", // Replace with your email password
  },
});

let win;
let datastore;
const encryptionKey = CryptoJS.lib.WordArray.random(256 / 8).toString();
const autosaveInterval = 10000; // Auto-save every 10 seconds 
let autosaveTimer = null;
let notesData = []; // Variable to store notes data




function createWindow() {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true,
    },
    autoHideMenuBar: true,
  });

  win.loadFile(path.join(__dirname, "index.html"));

  ipcMain.handle('dark-mode:toggle', () => {
    if (nativeTheme.shouldUseDarkColors) {
      nativeTheme.themeSource = 'light';
    } else {
      nativeTheme.themeSource = 'dark';
    }
    return nativeTheme.shouldUseDarkColors;
  });

  win.once("ready-to-show", () => {
    win.show();
  });

  win.on("focus", () => {
    startAutoSave();
  });

  win.on("blur", () => {
    stopAutoSave();
  });

  win.on("closed", () => {
    win = null; // Set the win variable to null when the window is closed
  });

  return win;
}

function initDatastore() {
  const userDataPath = app.getPath("userData");
  const dbPath = path.join(userDataPath, "notes.db");

  datastore = new Datastore({
    filename: dbPath,
    autoload: true,
    timestampData: true,
    onload: (err) => {
      if (err) {
        console.error("Error loading the datastore:", err);
        throw err;
      } else {
        console.log("Datastore loaded successfully.");

        // Load the notes from the datastore after it's successfully loaded
        datastore.find({}, (err, notes) => {
          if (err) {
            console.error("Error retrieving notes:", err);
          } else {
            notesData = notes;
          }
        });
      }
    },
  });
}

function getDataToExport() {
  return new Promise((resolve, reject) => {
    datastore.find({}, (err, notes) => {
      if (err) {
        console.error("Error retrieving notes for export:", err);
        reject(err);
      } else {
        // Map the notes and decrypt their titles and notes
        const title = notes.map((note) => decryptData(note.title)).join('\n');
        const noteContent = notes.map((note) => decryptData(note.note)).join('\n');

        resolve({ title, noteContent });
      }
    });
  });
}

function saveNotesToDatastore() {
  datastore.remove({}, { multi: true }, (err, numRemoved) => {
    if (err) {
      console.error("Error removing notes:", err);
    } else {
      datastore.insert(notesData, (err, docs) => {
        if (err) {
          console.error("Error inserting notes:", err);
        } else {
          console.log("Notes saved successfully.");

          // Send the updated notes data to the renderer process after autosaving
          if (win) {
            win.webContents.send('autosaved_notes', notesData);
          }
        }
      });
    }
  });
}

function startAutoSave() {
  if (!autosaveTimer) {
    autosaveTimer = setInterval(() => {
      datastore.find({}, (err, notes) => {
        if (err) {
          console.error("Error retrieving autosaved notes:", err);
        } else {
          notesData = notes;
          if (win) {
            win.webContents.send('autosaved_notes', notesData);
          }
          saveNotesToDatastore(); // Save the autosaved notes to the datastore
        }
      });
    }, autosaveInterval);
  }
}

function stopAutoSave() {
  if (autosaveTimer) {
    clearInterval(autosaveTimer);
    autosaveTimer = null;
  }
}

app.whenReady().then(() => {
  win = createWindow();
  initDatastore();
  startAutoSave(); // Start auto-saving notes data

  // Register global keyboard shortcuts
  globalShortcut.register('CommandOrControl+N', () => {
    win.webContents.send('new_note');
  });

  globalShortcut.register('CommandOrControl+S', () => {
    win.webContents.send('save_note');
  });

  globalShortcut.register('CommandOrControl+D', () => {
    win.webContents.send('delete_note');
  });
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("before-quit", () => {
  saveNotesToDatastore(); // Save notes before the application quits
});

ipcMain.on("save_note", (event, updatedNotes) => {
  notesData = updatedNotes; // Update the notesData array with the updated notes
  saveNotesToDatastore();
});

ipcMain.handle("get_data", async () => {
  return new Promise((resolve, reject) => {
    resolve(notesData); // Resolve with the notesData array
  });
});

ipcMain.on("share_note", (event, { note, email }) => {
  const mailOptions = {
    from: "alisedsmith@gmail.com",
    to: email, // Use the specified recipient email
    subject: "Shared Note",
    text: `Here is the shared note:\n\nTitle: ${note.title}\nNote: ${note.note}`,
    attachments: [
      {
        filename: "note.txt",
        content: `${note.title}\n\n${note.note}`,
      },
    ],
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log("Error sending email:", error);
      event.returnValue = false;
    } else {
      console.log("Email sent:", info.response);
      event.returnValue = true;
    }
  });
});

ipcMain.on("import_note", (event) => {
  dialog
    .showOpenDialog(win, {
      properties: ["openFile"],
      filters: [
        { name: "Text Files", extensions: ["txt"] },
        { name: "All Files", extensions: ["*"] },
      ],
    })
    .then((result) => {
      if (!result.canceled && result.filePaths.length > 0) {
        const filePath = result.filePaths[0];
        fs.readFile(filePath, "utf-8", async (err, data) => {
          if (err) {
            console.error("Error reading file:", err);
            return;
          }

          // Now you have the 'data' variable containing the content of the imported file
          // You can send this data back to the renderer process if needed

          // For example, sending the data back to the renderer process:
          event.sender.send("imported_note_content", data);
        });
      }
    })
    .catch((err) => {
      console.error("Error opening dialog:", err);
    });
});

ipcMain.on("export_note", (event) => {
  dialog.showSaveDialog(win, {
    defaultPath: "notes.txt",
    filters: [
      { name: "Text Files", extensions: ["txt"] },
      { name: "All Files", extensions: ["*"] },
    ],
  }).then(async (result) => {
    if (!result.canceled && result.filePath) {
      const filePath = result.filePath;
      const { title, noteContent } = await getDataToExport(); // Implement a function to get the data to export

      // Now you have the 'title' and 'noteContent' variables containing the data to export
      // You can write these data to the file (e.g., using fs.writeFile)

      // For example:
      const content = `${title}\n${noteContent}`;
      fs.writeFile(filePath, content, "utf-8", (err) => {
        if (err) {
          console.error("Error writing to file:", err);
          event.sender.send("exported_notes", false); // Notify the renderer process about the failure
        } else {
          console.log("Notes exported successfully.");
          event.sender.send("exported_notes", true); // Notify the renderer process about the success
        }
      });
    }
  });
});

ipcMain.on("backup_notes", (event) => {
  dialog.showSaveDialog(win, {
    defaultPath: "notes_backup.json",
    filters: [{ name: "JSON Files", extensions: ["json"] }],
  }).then((result) => {
    if (!result.canceled && result.filePath) {
      const filePath = result.filePath;
      fs.writeFile(filePath, JSON.stringify(notes), "utf-8", (err) => {
        if (err) {
          console.error("Error writing to backup file:", err);
          return;
        }
        console.log("Notes backed up successfully.");
      });
    }
  });
});

ipcMain.on("restore_notes", (event) => {
  dialog.showOpenDialog(win, {
    properties: ["openFile"],
    filters: [{ name: "JSON Files", extensions: ["json"] }],
  }).then((result) => {
    if (!result.canceled && result.filePaths.length > 0) {
      const filePath = result.filePaths[0];
      fs.readFile(filePath, "utf-8", async (err, data) => {
        if (err) {
          console.error("Error reading backup file:", err);
          return;
        }

        try {
          const restoredNotes = JSON.parse(data);
          // Update the 'notes' variable with the restored data
          notes = restoredNotes;
          // Send the restored notes data back to the renderer process
          event.sender.send("restored_notes", notes);
          console.log("Notes restored successfully.");
        } catch (error) {
          console.error("Error parsing backup data:", error);
        }
      });
    }
  });
});