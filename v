// const { app, BrowserWindow, ipcMain, nativeTheme, globalShortcut } = require("electron");
// const Datastore = require("nedb");
// const path = require("path");
// const nodemailer = require("nodemailer");
// const CryptoJS = require('crypto-js');
// const fs = require('fs');
// const { dialog } = require("electron");

// const transporter = nodemailer.createTransport({
//   service: "Gmail", // Replace with your email service provider
//   auth: {
//     user: "alisedsmith@gmail.com", // Replace with your email address
//     pass: "Khan7860", // Replace with your email password
//   },
// });

// let win;
// let datastore;
// const encryptionKey = CryptoJS.lib.WordArray.random(256 / 8).toString();
// const autosaveInterval = 10000; // Auto-save every 10 seconds 
// let autosaveTimer = null;
// let notesData = []; // Variable to store notes data




// function createWindow() {
//   win = new BrowserWindow({
//     width: 800,
//     height: 600,
//     webPreferences: {
//       nodeIntegration: true,
//       contextIsolation: false,
//       enableRemoteModule: true,
//     },
//     autoHideMenuBar: true,
//   });

//   win.loadFile(path.join(__dirname, "index.html"));

//   ipcMain.handle('dark-mode:toggle', () => {
//     if (nativeTheme.shouldUseDarkColors) {
//       nativeTheme.themeSource = 'light';
//     } else {
//       nativeTheme.themeSource = 'dark';
//     }
//     return nativeTheme.shouldUseDarkColors;
//   });

//   win.once("ready-to-show", () => {
//     win.show();
//   });

//   win.on("focus", () => {
//     startAutoSave();
//   });

//   win.on("blur", () => {
//     stopAutoSave();
//   });

//   win.on("closed", () => {
//     win = null; // Set the win variable to null when the window is closed
//   });

//   return win;
// }

// function initDatastore() {
//   const userDataPath = app.getPath("userData");
//   const dbPath = path.join(userDataPath, "notes.db");

//   datastore = new Datastore({
//     filename: dbPath,
//     autoload: true,
//     timestampData: true,
//     onload: (err) => {
//       if (err) {
//         console.error("Error loading the datastore:", err);
//         throw err;
//       } else {
//         console.log("Datastore loaded successfully.");

//         // Load the notes from the datastore after it's successfully loaded
//         datastore.find({}, (err, notes) => {
//           if (err) {
//             console.error("Error retrieving notes:", err);
//           } else {
//             notesData = notes;
//           }
//         });
//       }
//     },
//   });
// }

// function getDataToExport() {
//   return new Promise((resolve, reject) => {
//     datastore.find({}, (err, notes) => {
//       if (err) {
//         console.error("Error retrieving notes for export:", err);
//         reject(err);
//       } else {
//         // Map the notes and decrypt their titles and notes
//         const title = notes.map((note) => decryptData(note.title)).join('\n');
//         const noteContent = notes.map((note) => decryptData(note.note)).join('\n');

//         resolve({ title, noteContent });
//       }
//     });
//   });
// }

// function saveNotesToDatastore() {
//   datastore.remove({}, { multi: true }, (err, numRemoved) => {
//     if (err) {
//       console.error("Error removing notes:", err);
//     } else {
//       datastore.insert(notesData, (err, docs) => {
//         if (err) {
//           console.error("Error inserting notes:", err);
//         } else {
//           console.log("Notes saved successfully.");

//           // Send the updated notes data to the renderer process after autosaving
//           if (win) {
//             win.webContents.send('autosaved_notes', notesData);
//           }
//         }
//       });
//     }
//   });
// }

// function startAutoSave() {
//   if (!autosaveTimer) {
//     autosaveTimer = setInterval(() => {
//       datastore.find({}, (err, notes) => {
//         if (err) {
//           console.error("Error retrieving autosaved notes:", err);
//         } else {
//           notesData = notes;
//           if (win) {
//             win.webContents.send('autosaved_notes', notesData);
//           }
//           saveNotesToDatastore(); // Save the autosaved notes to the datastore
//         }
//       });
//     }, autosaveInterval);
//   }
// }

// function stopAutoSave() {
//   if (autosaveTimer) {
//     clearInterval(autosaveTimer);
//     autosaveTimer = null;
//   }
// }

// app.whenReady().then(() => {
//   win = createWindow();
//   initDatastore();
//   startAutoSave(); // Start auto-saving notes data

//   // Register global keyboard shortcuts
//   globalShortcut.register('CommandOrControl+N', () => {
//     win.webContents.send('new_note');
//   });

//   globalShortcut.register('CommandOrControl+S', () => {
//     win.webContents.send('save_note');
//   });

//   globalShortcut.register('CommandOrControl+D', () => {
//     win.webContents.send('delete_note');
//   });
// });

// app.on("window-all-closed", () => {
//   if (process.platform !== "darwin") {
//     app.quit();
//   }
// });

// app.on("before-quit", () => {
//   saveNotesToDatastore(); // Save notes before the application quits
// });

// ipcMain.on("save_note", (event, updatedNotes) => {
//   notesData = updatedNotes; // Update the notesData array with the updated notes
//   saveNotesToDatastore();
// });

// ipcMain.handle("get_data", async () => {
//   return new Promise((resolve, reject) => {
//     resolve(notesData); // Resolve with the notesData array
//   });
// });

// ipcMain.on("share_note", (event, { note, email }) => {
//   const mailOptions = {
//     from: "alisedsmith@gmail.com",
//     to: email, // Use the specified recipient email
//     subject: "Shared Note",
//     text: `Here is the shared note:\n\nTitle: ${note.title}\nNote: ${note.note}`,
//     attachments: [
//       {
//         filename: "note.txt",
//         content: `${note.title}\n\n${note.note}`,
//       },
//     ],
//   };

//   transporter.sendMail(mailOptions, (error, info) => {
//     if (error) {
//       console.log("Error sending email:", error);
//       event.returnValue = false;
//     } else {
//       console.log("Email sent:", info.response);
//       event.returnValue = true;
//     }
//   });
// });

// ipcMain.on("import_note", (event) => {
//   dialog
//     .showOpenDialog(win, {
//       properties: ["openFile"],
//       filters: [
//         { name: "Text Files", extensions: ["txt"] },
//         { name: "All Files", extensions: ["*"] },
//       ],
//     })
//     .then((result) => {
//       if (!result.canceled && result.filePaths.length > 0) {
//         const filePath = result.filePaths[0];
//         fs.readFile(filePath, "utf-8", async (err, data) => {
//           if (err) {
//             console.error("Error reading file:", err);
//             return;
//           }

//           // Now you have the 'data' variable containing the content of the imported file
//           // You can send this data back to the renderer process if needed

//           // For example, sending the data back to the renderer process:
//           event.sender.send("imported_note_content", data);
//         });
//       }
//     })
//     .catch((err) => {
//       console.error("Error opening dialog:", err);
//     });
// });

// ipcMain.on("export_note", (event) => {
//   dialog.showSaveDialog(win, {
//     defaultPath: "notes.txt",
//     filters: [
//       { name: "Text Files", extensions: ["txt"] },
//       { name: "All Files", extensions: ["*"] },
//     ],
//   }).then(async (result) => {
//     if (!result.canceled && result.filePath) {
//       const filePath = result.filePath;
//       const { title, noteContent } = await getDataToExport(); // Implement a function to get the data to export

//       // Now you have the 'title' and 'noteContent' variables containing the data to export
//       // You can write these data to the file (e.g., using fs.writeFile)

//       // For example:
//       const content = `${title}\n${noteContent}`;
//       fs.writeFile(filePath, content, "utf-8", (err) => {
//         if (err) {
//           console.error("Error writing to file:", err);
//           event.sender.send("exported_notes", false); // Notify the renderer process about the failure
//         } else {
//           console.log("Notes exported successfully.");
//           event.sender.send("exported_notes", true); // Notify the renderer process about the success
//         }
//       });
//     }
//   });
// });

// ipcMain.on("backup_notes", (event) => {
//   dialog.showSaveDialog(win, {
//     defaultPath: "notes_backup.json",
//     filters: [{ name: "JSON Files", extensions: ["json"] }],
//   }).then((result) => {
//     if (!result.canceled && result.filePath) {
//       const filePath = result.filePath;
//       fs.writeFile(filePath, JSON.stringify(notes), "utf-8", (err) => {
//         if (err) {
//           console.error("Error writing to backup file:", err);
//           return;
//         }
//         console.log("Notes backed up successfully.");
//       });
//     }
//   });
// });

// ipcMain.on("restore_notes", (event) => {
//   dialog.showOpenDialog(win, {
//     properties: ["openFile"],
//     filters: [{ name: "JSON Files", extensions: ["json"] }],
//   }).then((result) => {
//     if (!result.canceled && result.filePaths.length > 0) {
//       const filePath = result.filePaths[0];
//       fs.readFile(filePath, "utf-8", async (err, data) => {
//         if (err) {
//           console.error("Error reading backup file:", err);
//           return;
//         }

//         try {
//           const restoredNotes = JSON.parse(data);
//           // Update the 'notes' variable with the restored data
//           notes = restoredNotes;
//           // Send the restored notes data back to the renderer process
//           event.sender.send("restored_notes", notes);
//           console.log("Notes restored successfully.");
//         } catch (error) {
//           console.error("Error parsing backup data:", error);
//         }
//       });
//     }
//   });
// });













// const { ipcRenderer, remote } = require("electron");
// const { v4: uuidv4 } = require('uuid');
// const CryptoJS = require('crypto-js');
// const fs = require("fs");


// const titleInput = document.getElementById("title");
// const noteInput = new Quill("#note");
// const saveBtn = document.getElementById("btn");
// const shareBtn = document.getElementById("share-btn");
// const noteList = document.getElementById("list");
// const titleError = document.getElementById("titleError");
// const noteError = document.getElementById("noteError");
// // const encryptionKey = CryptoJS.lib.WordArray.random(256 / 8).toString();

// let notes = [];
// let editingIndex = -1;

// function loadNotes() {
//   noteList.innerHTML = "";
//   notes.forEach((note, idx) => {
//     noteList.innerHTML += `
//       <div class="list_ele">
//         <h1>${idx} ${decryptData(note.title)}</h1>
//         <p>${decryptData(note.note)}</p>
//         <button class="edit-btn" data-index="${idx}">Edit</button>
//         <button class="delete-btn" data-index="${idx}">Delete</button>
//       </div>
//     `;
//   });

//   // Add event listeners to edit and delete buttons
//   const editButtons = document.querySelectorAll(".edit-btn");
//   const deleteButtons = document.querySelectorAll(".delete-btn");

//   editButtons.forEach((button) => {
//     button.addEventListener("click", handleEdit);
//   });

//   deleteButtons.forEach((button) => {
//     button.addEventListener("click", handleDelete);
//   });
// }

// window.addEventListener('DOMContentLoaded', async () => {
//   notes = await ipcRenderer.invoke("get_data");
//   loadNotes();

//   // Handle new_note global shortcut from the main process
//   ipcRenderer.on('new_note', () => {
//     titleInput.value = '';
//     noteInput.setText('');
//     editingIndex = -1;
//     saveBtn.textContent = "Save";
//   });

//   // Handle save_note global shortcut from the main process
//   ipcRenderer.on('save_note', () => {
//     saveNote();
//   });

//   // Handle delete_note global shortcut from the main process
//   ipcRenderer.on('delete_note', () => {
//     if (editingIndex !== -1) {
//       notes.splice(editingIndex, 1);
//       loadNotes();
//       ipcRenderer.send('save_note', notes);
//     }
//   });
// });

// saveBtn.addEventListener('click', () => {
//   saveNote();
// });

// shareBtn.addEventListener('click', () => {
//   shareNote();
// });



// // Update the notes data when receiving the 'autosaved_notes' message from the main process
// ipcRenderer.on('autosaved_notes', (event, updatedNotes) => {
//   notes = updatedNotes;
//   loadNotes(); // Update the UI with the updated notes data
// });




// function saveNote() {
//   const titleValue = titleInput.value.trim();
//   const noteValue = noteInput.getText().trim();

//   // Remove error messages
//   titleError.textContent = '';
//   noteError.textContent = '';

//   // Validate title
//   if (titleValue === '') {
//     titleError.textContent = 'Title is required';
//   } else if (titleValue.length > 10) {
//     titleError.textContent = 'Title should not exceed 10 characters';
//   } else if (titleValue.length < 5) {
//     titleError.textContent = 'Title must be 5 or more characters';
//   }

//   // Validate note
//   if (noteValue === '') {
//     noteError.textContent = 'Note is required';
//   } else if (noteValue.length > 5000) {
//     noteError.textContent = 'Note should not exceed 5000 characters';
//   }

//   // Save the note if there are no errors
//   if (titleError.textContent === '' && noteError.textContent === '') {
//     if (editingIndex !== -1) {
//       // Editing an existing note
//       notes[editingIndex].title = encryptData(titleValue);
//       notes[editingIndex].note = encryptData(noteValue);
//       editingIndex = -1;
//     } else {
//       // Adding a new note with a unique key
//       const newNote = {
//         key: uuidv4(),
//         title: encryptData(titleValue),
//         note: encryptData(noteValue),
//       };
//       notes.push(newNote);
//     }

//     titleInput.value = '';
//     noteInput.setText('');
//     loadNotes();

//     ipcRenderer.send('save_note', notes);
//   }
// }

// function shareNote() {
//   const titleValue = titleInput.value.trim();
//   const noteValue = noteInput.getText().trim();
//   const emailInput = document.getElementById("email").value.trim();

//   // Remove error messages
//   titleError.textContent = '';
//   noteError.textContent = '';

//   // Validate title
//   if (titleValue === '') {
//     titleError.textContent = 'Title is required';
//   } else if (titleValue.length > 10) {
//     titleError.textContent = 'Title should not exceed 10 characters';
//   } else if (titleValue.length < 5) {
//     titleError.textContent = 'Title must be 5 or more characters';
//   }

//   // Validate note
//   if (noteValue === '') {
//     noteError.textContent = 'Note is required';
//   } else if (noteValue.length > 5000) {
//     noteError.textContent = 'Note should not exceed 5000 characters';
//   }

//   // Validate email
//   if (!isValidEmail(emailInput)) {
//     noteError.textContent = 'Invalid email address';
//   }

//   // Share the note if there are no errors
//   if (titleError.textContent === '' && noteError.textContent === '') {
//     const note = {
//       title: titleValue,
//       note: noteValue
//     };

//     const isShared = ipcRenderer.sendSync("share_note", { note, email: emailInput });

//     if (isShared) {
//       alert("Note shared successfully!");
//     } else {
//       alert("Failed to share the note. Please try again later.");
//     }
//   }
// }

// function isValidEmail(email) {
//   // Simple email validation regex, you can use a more comprehensive one if needed
//   const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//   return emailRegex.test(email);
// }

// function handleEdit(event) {
//   const index = event.target.dataset.index;
//   const selectedNote = notes[index];

//   titleInput.value = decryptData(selectedNote.title);
//   noteInput.setText(decryptData(selectedNote.note));
//   editingIndex = index;

//   saveBtn.textContent = "Save";
// }



// function handleDelete(event) {
//   const index = event.target.dataset.index;
//   notes.splice(index, 1);
//   loadNotes();

//   ipcRenderer.send('save_note', notes);
// }

// document.getElementById('toggle-dark-mode').addEventListener('click', async () => {
//   const isDarkMode = await ipcRenderer.invoke('dark-mode:toggle');
//   document.getElementById('theme-source').innerHTML = isDarkMode ? 'Dark' : 'Light';
// });

// // Generate encryption key
// const keySizeInBits = 256; // Length of the encryption key in bits (e.g., 128, 256)
// const encryptionKey = CryptoJS.lib.WordArray.random(keySizeInBits / 8).toString();

// // Encryption function
// function encryptData(data) {
//   const encryptedData = CryptoJS.AES.encrypt(data, encryptionKey).toString();
//   return encryptedData;
// }

// // Decryption function
// function decryptData(encryptedData) {
//   try {
//     const decryptedData = CryptoJS.AES.decrypt(encryptedData, encryptionKey).toString(CryptoJS.enc.Utf8);
//     return decryptedData;
//   } catch (error) {
//     console.error("Error decrypting data:", error);
//     return ""; // Return an empty string or handle the error in an appropriate way
//   }
// }


// function importNotes() {
//   const { dialog } = require("electron").remote;

//   dialog.showOpenDialog(
//     {
//       properties: ["openFile"],
//       filters: [
//         { name: "Text Files", extensions: ["txt"] },
//         { name: "PDF Files", extensions: ["pdf"] },
//         { name: "CSV Files", extensions: ["csv"] },
//         { name: "All Files", extensions: ["*"] },
//       ],
//     },
//     (filePaths) => {
//       if (filePaths && filePaths.length > 0) {
//         const fs = require("fs");
//         const path = filePaths[0];
//         const fileExtension = path.split(".").pop().toLowerCase();

//         fs.readFile(path, "utf-8", async (err, data) => {
//           if (err) {
//             console.error("Error reading file:", err);
//             return;
//           }

//           console.log("Data read from file:", data);

//           // Set the content of the Quill editor with the imported data
//           quill.root.innerHTML = data;

//           // Set the title as the filename without extension
//           titleInput.value = path.split("/").pop().split(".")[0];
//         });
//       }
//     }
//   );
// }




// function exportNotes() {
//   const { dialog } = require("electron").remote;

//   dialog.showSaveDialog(
//     {
//       defaultPath: "notes.txt",
//       filters: [
//         { name: "text/plain;charset=utf-8;", extensions: ["txt"] },
//         { name: "application/pdf", extensions: ["pdf"] },
//         { name: "text/csv;charset=utf-8;", extensions: ["csv"] },
//       ],
//     },
//     (filePath) => {
//       if (filePath) {
//         const fs = require("fs");
//         const fileExtension = filePath.split(".").pop().toLowerCase();

//         // Get the note content from the Quill editor as HTML
//         const noteContent = noteInput.root.innerHTML;

//         // Prepare the data based on the selected file format
//         let data;
//         if (fileExtension === "pdf") {
//           // Export as PDF
//           const PDFDocument = require("pdfkit");
//           const pdfDoc = new PDFDocument();
//           pdfDoc.pipe(fs.createWriteStream(filePath));
//           pdfDoc.font("Helvetica-Bold").text(encryptData(titleInput.value), {
//             align: "center",
//           });
//           pdfDoc.font("Helvetica").text(noteContent);
//           pdfDoc.end();
//         } else {
//           // Export as text or CSV
//           data = noteContent;
//           fs.writeFile(filePath, data, "utf-8", (err) => {
//             if (err) {
//               console.error("Error writing to file:", err);
//               return;
//             }
//             console.log("Notes exported successfully.");
//           });
//         }
//       }
//     }
//   );
// }


// // In the renderer process JavaScript
// const importBtn = document.getElementById("import-btn");
// const exportBtn = document.getElementById("export-btn");

// importBtn.addEventListener("click", importNotes);
// exportBtn.addEventListener("click", exportNotes);

// ipcRenderer.on("imported_note_content", (event, data) => {
//   console.log("Received imported note content:", data); // Add this debug log

//   // Set the content of the Quill editor with the imported data
//   quill.root.innerHTML = data;
// });


// // Event listener for the "Backup" button
// document.getElementById("backup-btn").addEventListener("click", () => {
//   backupNotes();
// });

// // Event listener for the "Restore" button
// document.getElementById("restore-btn").addEventListener("click", () => {
//   restoreNotes();
// });


// function backupNotes() {
//   // Show a save dialog to select the backup file location
//   const { dialog } = remote;
//   dialog.showSaveDialog(
//     {
//       defaultPath: "notes_backup.json",
//       filters: [{ name: "JSON Files", extensions: ["json"] }],
//     },
//     (filePath) => {
//       if (filePath) {
//         // Save the notes data to the selected file location
//         const dataToBackup = JSON.stringify(notes);
//         fs.writeFile(filePath, dataToBackup, "utf-8", (err) => {
//           if (err) {
//             console.error("Error writing to backup file:", err);
//             return;
//           }
//           console.log("Notes backed up successfully.");
//         });
//       }
//     }
//   );
// }



// function restoreNotes() {
//   // Show an open dialog to select the backup file to restore from
//   const { dialog } = remote;
//   dialog.showOpenDialog(
//     {
//       properties: ["openFile"],
//       filters: [{ name: "JSON Files", extensions: ["json"] }],
//     },
//     (filePaths) => {
//       if (filePaths && filePaths.length > 0) {
//         const filePath = filePaths[0];
//         fs.readFile(filePath, "utf-8", async (err, data) => {
//           if (err) {
//             console.error("Error reading backup file:", err);
//             return;
//           }

//           try {
//             const restoredNotes = JSON.parse(data);
//             // Update the 'notes' variable with the restored data
//             notes = restoredNotes;
//             loadNotes();
//             console.log("Notes restored successfully.");
//           } catch (error) {
//             console.error("Error parsing backup data:", error);
//           }
//         });
//       }
//     }
//   );
// }

// // Event listener to handle the restored notes data from the main process
// ipcRenderer.on("restored_notes", (event, restoredNotes) => {
//   // Update the 'notes' variable with the restored data
//   notes = restoredNotes;
//   // Load the restored notes data into the UI
//   loadNotes();
// });




// const dropZone = document.querySelector('.quill-drop-zone');

// function handleDrop(e) {
//   e.preventDefault();
//   e.stopPropagation();

//   const file = e.dataTransfer.files[0];
//   if (file) {
//     const reader = new FileReader();
//     reader.onload = () => {
//       const content = reader.result;
//       const range = quill.getSelection(true);
//       quill.clipboard.dangerouslyPasteHTML(range.index, content);
//     };
//     reader.readAsText(file);
//   }

//   dropZone.style.display = 'none';
// }

// function handleDragOver(e) {
//   e.preventDefault();
//   e.stopPropagation();

//   dropZone.style.display = 'block';
// }

// function handleDragLeave(e) {
//   e.preventDefault();
//   e.stopPropagation();

//   dropZone.style.display = 'none';
// }

// quill.container.addEventListener('drop', (e) => {
//   e.preventDefault();
// });

// dropZone.addEventListener('drop', handleDrop);
// dropZone.addEventListener('dragover', handleDragOver);
// dropZone.addEventListener('dragleave', handleDragLeave);

















// <!DOCTYPE html>
// <html lang="en">
// <head>
//   <meta charset="UTF-8" />
//   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
//   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
//   <title>Note Taking App</title>
//   <link rel="stylesheet" href="style.css" />
//   <style>
//     .error {
//       color: red;
//       font-size: 0.8rem;
//     }
//   </style>
//   <!-- Include Quill.js CSS -->
//   <link href="https://cdn.quilljs.com/1.3.7/quill.snow.css" rel="stylesheet">
// </head>
// <body>
//   <div class="header-actions">
//     <h3>Theme: <strong id="theme-source"></strong></h3>
//     <button id="toggle-dark-mode">Dark/Light</button>

//   </div>

//   <div class="nav">Note Taking App</div>
//   <h2 style="text-align: center;">Add a New Note</h2>
//   <div class="note_title">
//     <label for="title">Title</label>
//     <input type="text" id="title" placeholder="Title of the note" required />
//     <div id="titleError" class="error"></div>
//   </div>
//   <hr>
  
//   <div>
    
//     <!-- Replace the textarea with Quill editor container -->
//     <textarea id="note" style="height: 300px;">
//     <!-- Add a class "quill-drop-zone" to enable drop functionality -->
//   <div class="quill-drop-zone">Drop files here to insert into the editor</div>
// </textarea>
//     <div id="noteError" class="error"></div>
    
//     <div>
//       <label for="email">Recipient Email</label>
//       <input type="email" id="email" placeholder="Enter recipient's email" required />
//     </div>
    
//     <button id="share-btn">Share</button>
//     <button id="btn">Save</button>
//     <button id="import-btn">Import</button>
//     <button id="export-btn">Export</button>
//       <!-- New buttons for backup and restore -->
//   <button id="backup-btn">Backup</button>
//   <button id="restore-btn">Restore</button>
//   </div>
  
//   <!-- Include Quill.js JavaScript -->
//   <script src="https://cdn.quilljs.com/1.3.7/quill.js"></script>
//   <div id="list"></div>
//   <script>
//     // Initialize Quill editor with file upload support
//     var quill = new Quill('#note', {
//       theme: 'snow',
//       modules: {
//         toolbar: [
//           ['bold', 'italic', 'underline', 'strike'],
//           ['link', 'image', 'video'],
//           [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
//           [{ 'color': [] }, { 'background': [] }],
//           [{ 'list': 'ordered' }, { 'list': 'bullet' }],
//           ['clean']
//         ],
//         clipboard: {
//           matchVisual: false
//         },
//         history: {
//           delay: 1000,
//           maxStack: 500,
//           userOnly: true
//         }
//       }
//     });
//   </script>
//   <script src="./renderer.js"></script>
//   <script src="./crypto-js.js"></script>

// </body>
// </html>
